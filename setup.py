import setuptools

setuptools.setup(
	name = 'example_fastapi_app',
	version="0.0.1",
	author="ID",
        author_email="mymail@gmail.com",
        packages=setuptools.find_packages(),
        classifiers=[
        "Programming Language :: Python :: 3",
        "License :: OSI Approved :: MIT License",
        "Operating System :: OS Independent",
    ]
)

