from . import main

def test_create_record():
    assert main.create_record("itamar", 23, "fat") == None

def test_get_record():
    assert main.get_record("itamar").name == \
        main.person("itamar", 23, "fat").name