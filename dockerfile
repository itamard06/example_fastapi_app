FROM python:3.9-alpine

RUN pip install example_fastapi_app-0.0.1-py3-none-any.whl

ENTRYPOINT ["python3", "-m example_fastapi_app.main"]